﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Droides.Droides;

namespace Droides
{
    class Astromecanicos : Droides
    {

        public string modelo; // R2-D2, BB-8

        public void setModelo(string _modelo)
        {
            this.modelo = _modelo;
        }

        public void getModelo()
        {
            Console.WriteLine("Modelo es: " + this.modelo);
        }
        public override void getInformacion()
        {
            Console.WriteLine("Droides Atromecánicos"); 
        }

        public void getAstromecanico()
        {
            Console.WriteLine("                       ");
            Console.WriteLine("        .---.          ");
            Console.WriteLine("      .'_:___\".        ");
            Console.WriteLine("      |__ --==|        ");
            Console.WriteLine("      [  ]  :[|        ");
            Console.WriteLine("      |__| I=[|        ");
            Console.WriteLine("      / / ____|        ");
            Console.WriteLine("     |-/.____.'        ");
            Console.WriteLine("    /___\\ /___\\        ");
            Console.WriteLine("                       ");
        }

    }
}
