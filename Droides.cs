﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Droides
{
    /// <summary>
    /// Clase abstracta Droides
    /// Atributos generales de los droides
    /// </summary>
    abstract class Droides
    {
        // Atributos
        public string fabricante;
        public string altura;
        public string equipamiento;

        // Droides() { }

        // Droides(string _f, string _a, string _e) {
        //     this.fabricante = _f;
        //     this.altura = _a;
        //     this.equipamiento = _e;
        // }

        public void getInfoDroide()
        {
            Console.WriteLine("Droide");
            Console.WriteLine("Fabricante " + this.fabricante);
            Console.WriteLine("Altura " + this.altura);
            Console.WriteLine("Equipamiento " + this.equipamiento );
        }

        public abstract void getInformacion();
    }
}
