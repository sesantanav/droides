﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Droides.Astromecanicos;
using static Droides.Protocolo;

namespace Droides
{
    class Factory 
    {
        public Droides getDroide(string _tipo)
        {
            if (_tipo.Equals("Astromecanico"))
            {
                return new Astromecanicos();
            }
            else // (_tipo.Equals("Protocolo"))
            {
                return new Protocolo();
            }
        }
    }
}
