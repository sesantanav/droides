﻿using System;

using static Droides.Factory;

namespace Droides
{
    class Program
    {
        static void Main(string[] args)
        {
            Droides droide1 = new Factory().getDroide("Astromecanico");

            droide1.getInformacion();
            
            Astromecanicos droide2  = new Astromecanicos();
            droide2.setModelo();


            Droides droide2 = new Factory().getDroide("Protocolo");
            droide2.getInformacion();

        }
    }
}
